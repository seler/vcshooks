#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup

version = __import__('vcshooks').__version__

setup(
    name='vcshooks',
    version=version,
    author='Rafał Selewońko',
    author_email='rafal@selewonko.com',
    description='A set of useful git and hg hooks. Both for developer and server side use.',
    url='http://bitbucket.org/seler/vcshooks',
    packages=['vcshooks'],
    license='MIT',
    entry_points={'console_scripts': [
        'vcshooks = vcshooks.commandline:main',
    ]},
    long_description=open('README.rst').read(),
)
