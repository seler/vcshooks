#!/usr/bin/env python
# -*- coding: utf-8 -*-

u"""
Commandline management script
"""

__author__ = u"Rafał Selewońko <rafal@selewonko.com>"

import argparse
import os

from vcshooks.vcslayer import VCS_CHOICES

BACKUP_HOOKS_DIR = '.vcshooks/backup'


def find_repository(directory, vcs):
    for f in os.listdir(directory):
        if os.path.isdir(os.path.join(directory, f)) and f == vcs.repo_dir_name:
            return f


def find_vcs(repository_candidate, vcs=None):
    if vcs is None:
        vcs = zip(*VCS_CHOICES)[1]

    for i in range(25):
        for v in vcs:
            repository = find_repository(repository_candidate, v)
            if repository:
                return v(os.path.join(repository_candidate, repository))

        repository_candidate = os.path.realpath(os.path.join(repository_candidate, '..'))


def setup(args):
    repository_candidate = os.path.realpath(os.path.expanduser(args.repository))
    vcs = None
    if args.vcs:
        vcs = args.vcs

    repository = find_vcs(repository_candidate, vcs)
    if repository:
        # copy current hooks to backup dir
        repository.create_backup(BACKUP_HOOKS_DIR)
        # TODO: install multihook for all hooks
        repository.install_multihook()
        # TODO: create dirs for multihook
        # TODO: copy old hooks into poper multihook dirs


def install(args):
    print args


def vcs(string):
    for c, t in VCS_CHOICES:
        if string == c:
            return t
    raise argparse.ArgumentError()


def main():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()
    setup_parser = subparsers.add_parser('setup')
    setup_parser.add_argument('--vcs', nargs=1, type=vcs)
    setup_parser.add_argument('repository', default='.', nargs='?')
    setup_parser.set_defaults(func=setup)

    install_parser = subparsers.add_parser('install')
    install_parser.set_defaults(func=install)

    args = parser.parse_args()
    args.func(args)
