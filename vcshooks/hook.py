#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Hook(object):

    def run(self):
        raise NotImplementedError
