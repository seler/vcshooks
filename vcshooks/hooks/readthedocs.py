import urllib2
import sys
from vcshooks.hook import Hook
from vcshooks.vcslayer import GitHook, MercurialHook


class ReadTheDocs(Hook):
    url = "http://readthedocs.org/build/{project}/"

    def run(self, project):
        url = self.url.format(project=project)

        def get_method():
            return "POST"

        handler = urllib2.HTTPHandler()
        opener = urllib2.build_opener(handler)
        request = urllib2.Request(url)
        request.get_method = get_method

        try:
            connection = opener.open(request)
        except urllib2.HTTPError, e:
            connection = e

        if connection.code != 200:
            sys.stderr.write("Could not trigger ReadTheDocs build. ")
            sys.stderr.write(connection.read())
            sys.stderr.write("\n")


git_hook = GitHook(ReadTheDocs)
hg_hook = MercurialHook(ReadTheDocs)
