#!/usr/bin/env python
# -*- coding: utf-8 -*-

u"""
Runs all hooks that are saved under <current_name>-hooks/*
For example if this file is saved as .git/hooks/pre-commit
it executes all files found in .git/hooks/pre-commit-hooks/
"""

import os
import sys
import subprocess
from vcshooks.const import STDIN_GIT_HOOKS
from vcshooks.vcslayer import GitHook, MercurialHook
from vcshooks.hook import Hook

MULTIPLE_HOOKS_DIR = "{hook}-hooks"


class MultiHook(Hook):

    def run(self, hook):
        self.hook = hook
        hooks_dir = MULTIPLE_HOOKS_DIR.format(hook=os.path.realpath(hook))
        filepaths = self.get_executable_files(hooks_dir)
        code = self.run_subhooks(filepaths)
        return code

    def get_executable_files(self, hooks_dir):
        filepaths = []
        for dirname, dirnames, filenames in os.walk(hooks_dir):
            for filename in filenames:
                filepath = os.path.join(dirname, filename)
                if os.access(filepath, os.X_OK):
                    filepaths.append(filepath)
        return filepaths

    def run_subhooks(self, filepaths):
        stdin = None
        if self.hook in STDIN_GIT_HOOKS:
            stdin = sys.stdin.read()

        args = sys.argv
        for filepath in sorted(filepaths):
            code = self.run_subhook(filepath, args, stdin)
            # stop execution when something goes wrong
            if code:
                return code
        return 0

    def run_subhook(self, filepath, args, stdin=None):
        cmd = [filepath] + args
        process = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE)
        process.communicate(input=stdin)
        return process.returncode


git_hook = GitHook(MultiHook)
hg_hook = MercurialHook(MultiHook)
