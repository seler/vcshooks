#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import shutil


class VcsHook(object):

    def __init__(self, hook):
        self.hook = hook

    def __call__(self, *args, **kwargs):
        return self.hook().run(*args, **kwargs)


class GitHook(VcsHook):
    pass


class MercurialHook(VcsHook):
    pass


class VCS(object):
    def __init__(self, repository):
        """TODO: Docstring for __init__.

        :repository: TODO
        :returns: TODO

        """
        self.vcsdir = repository
        self.repository = os.path.realpath(os.path.join(repository, os.path.pardir))

    def __str__(self):
        return self.repository

    def create_backup(self, backup_dirname):
        backup_dir = self.get_backup_dir(backup_dirname)
        os.makedirs(backup_dir)

        hooks_dir = self.get_hooks_dir()
        for f in os.listdir(hooks_dir):
            src = os.path.join(hooks_dir, f)
            dst = os.path.join(backup_dir, f)
            shutil.move(src, dst)

    def get_hooks_dir(self):
        return os.path.join(self.vcsdir, 'hooks')

    def get_backup_dir(self, dirname):
        return os.path.join(self.repository, dirname)


class Git(VCS):
    slug = 'git'
    repo_dir_name = '.git'


class Mercurial(VCS):
    slug = 'hg'
    repo_dir_name = '.hg'


VCS_CHOICES = (
    (Git.slug, Git),
    (Git.slug, Mercurial),
)
